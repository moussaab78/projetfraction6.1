//modif2
public class Fraction {
	//blah blah test commentaire
	private int numerator, denominator;
	//mkfnjcùQL?mùLQKD
	 public Fraction(){
	      numerator = denominator = 0;
	 }
	 public int getNumerator(){
	      return numerator;
	 }
	 public void setNumerator(int num){
	      numerator=num;
	 }
	 public int getDenominator(){
	      return denominator;
	 }
	 public void setDenominator(int den){
	      denominator=den;
	 }
	 public Fraction add(Fraction b){
	      if ((denominator == 0) || (b.denominator == 0))
	         throw new IllegalArgumentException("invalid denominator");
	      int common = lcd(denominator, b.denominator);
	      Fraction commonA = new Fraction();
	      Fraction commonB = new Fraction();
	      commonA = convert(common);
	      commonB = b.convert(common);
	      Fraction sum = new Fraction();
	      sum.numerator = commonA.numerator + commonB.numerator;
	      sum.denominator = common;
	      sum = sum.reduce();
	      return sum;
	   }
	 
	 
}
