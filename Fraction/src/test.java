
public class test {
	 public static void main(String args[]){
	      Fraction f1 = new Fraction();   // local fraction objects
	      Fraction f2 = new Fraction();   // used to test methods

	      // one way to set up fractions is simply to hard-code some values
	      f1.setNumerator(1);
	      f1.setDenominator(3);
	      f2.setNumerator(1);
	      f2.setDenominator(6);
	      
	      // try some arithmetic on these fractions
	      Fraction result = new Fraction();
	      // test addition
	      result = f1.add(f2);
	      // one way to output results, using toString method directly
	      System.out.println(f1 + " + " + f2 + " = " + result);
	      // test addition going the other way - should be same result
	      result = f2.add(f1);
	      // output results
	      System.out.println(f2 + " + " + f1 + " = " + result);
	      System.out.println();
	 }
}
